#ifndef UNIQUEINTS_HPP_
#define UNIQUEINTS_HPP_

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <numeric>
#include <stdexcept>
#include <vector>

#include "murmur3lite.hpp"
#include "reg_storage.hpp"

class UniqCounter {
public:
    UniqCounter(const int precis = 15)
        : b(precis),
          m(1 << precis),
          alpha_mm(choose_alpha(precis, m)),
          registers(m) {
        // relative error < 10%, mem < 32kb in [9..15]
        if (precis < 4 || 15 < precis) {
            throw std::invalid_argument("Precision should be in [4..15]");
        }
    }

    void add(int e) {
        const auto x = hash::murmurhash_for_int(e);
        const auto index = x >> (std::numeric_limits<uint32_t>::digits - b);
        registers.update_if_more(index, count_leading_zeros(x));
    }

    int get_uniq_num() const {
        const double pow_2_32 = (1ULL << 32), npow_2_32 = -pow_2_32;
        double sum = 0.0;
        for (uint32_t i = 0; i < m; ++i) {
            sum += 1.0 / (1 << registers.get(i));
        }
        const auto E = alpha_mm / sum;
        if (E <= 2.5 * m) {
            uint32_t zeros = 0;
            for (uint32_t i = 0; i < m; ++i) {
                if (registers.get(i) == 0) {
                    ++zeros;
                }
            }
            return zeros != 0 ? linear_counting(zeros) : E;
        }
        if (E > pow_2_32 / 30.0) {
            return npow_2_32 * std::log(1.0 - E / pow_2_32);
        }
        return int(E);
    }

private:
    inline double choose_alpha(uint8_t b, uint32_t m) const {
        switch (b) {
        case 4:
            return 0.673 * m * m;
        case 5:
            return 0.697 * m * m;
        case 6:
            return 0.709 * m * m;
        default:
            return (0.7213 / (1 + 1.079 / m)) * m * m;
        }
    }

    inline uint8_t count_leading_zeros(uint32_t x) const noexcept {
        x <<= b;
        uint8_t zeros = 1;
        while (zeros <= std::numeric_limits<uint32_t>::digits - b &&
               !(x & 0x80000000)) {
            x <<= 1;
            ++zeros;
        }
        return zeros;
    }

    inline int linear_counting(uint32_t zeros) const {
        return m * std::log(double(m) / zeros);
    }

    const uint8_t b;
    const uint32_t m;
    const double alpha_mm;
    storage::reg_storage registers;
};

#endif  // UNIQUE_COUNTER_HPP_
