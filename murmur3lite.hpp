#ifndef MURMUR3LITE_HPP_
#define MURMUR3LITE_HPP_

#include <cstdint>
#include <limits>

namespace hash {

const uint32_t HLL_MMH3_SEED = 313;

inline uint32_t rotl32(uint32_t x, uint8_t r) {
    return (x << r) | (x >> (std::numeric_limits<uint32_t>::digits - r));
}

inline uint32_t fmix32(uint32_t h) {
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
}

// https://en.wikipedia.org/wiki/MurmurHash
// truncated version of MurmurHash3
inline uint32_t murmurhash_for_int(uint32_t value,
                                   uint32_t seed = HLL_MMH3_SEED) {
    const uint32_t c1 = 0xcc9e2d51, c2 = 0x1b873593;
    const uint32_t k1 = rotl32(value * c1, 15) * c2;
    const uint32_t h1 = rotl32(seed ^ k1, 13);
    return fmix32((h1 * 5 + 0xe6546b64) ^ 4);
}

}  // namespace hash

#endif  // MURMUR3LITE_HPP_
