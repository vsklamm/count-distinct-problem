#include <cmath>
#include <limits>

#include "reg_storage.hpp"

namespace storage {

reg_storage::reg_storage(uint32_t registers)
    : buckets(std::ceil(double(registers) / REG_PER_BUCKET), 0) {}

uint8_t reg_storage::get(uint32_t index) const {
    const uint32_t real_index = index / REG_PER_BUCKET;
    const uint32_t offset = (index % REG_PER_BUCKET) * REG_SIZE;
    return 0b00011111 & buckets[real_index] >> offset;
}

void reg_storage::update_if_more(uint32_t index, uint8_t value) {
    const uint32_t real_index = index / REG_PER_BUCKET;
    const uint32_t offset = (index % REG_PER_BUCKET) * REG_SIZE;
    const uint8_t old_value = 0b00011111 & buckets[real_index] >> offset;
    if (old_value < value) {
        buckets[real_index] = (buckets[real_index] & ~(0b00011111 << offset)) |
                uint32_t(value) << offset;
    }
}

}  // namespace storage
