#ifndef REG_STORAGE_HPP_
#define REG_STORAGE_HPP_

#include <cstddef>
#include <cstdint>
#include <vector>

namespace storage {

const uint32_t REG_SIZE = 5;
const uint32_t REG_PER_BUCKET = 6;

class reg_storage {
public:
    reg_storage(uint32_t registers);

    uint8_t get(uint32_t index) const;
    void update_if_more(uint32_t index, uint8_t value);

private:
    std::vector<uint32_t> buckets;
};

}  // namespace storage

#endif  // REG_STORAGE_HPP_
