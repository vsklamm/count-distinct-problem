#include <cassert>
#include <iostream>
#include <iterator>
#include <random>
#include <set>
#include <unordered_set>

#include "unique_ints.hpp"

double relative_error(int expected, int got) {
    return abs(got - expected) / (double)expected;
}

int main() {
    std::random_device rd;
    std::mt19937 gen(rd());
    const int N = 1e6;

    std::vector<int> tests;
    for (int i = 1; i <= 40; ++i) {
        tests.push_back(50 * i);
    }
    for (int i = 1; i <= 50; ++i) {
        tests.push_back(6000 * i);
    }
    for (int i = 100000; i <= 1e8; i *= 2) {
        tests.push_back(i);
    }

    std::set<double> all_errors;
    for (size_t i = 0; i < tests.size(); ++i) {
        std::uniform_int_distribution<> dis(1, tests[i]);
        std::unordered_set<int> all;
        UniqCounter counter;
        for (int j = 0; j < N; ++j) {
            int value = dis(gen);
            all.insert(value);
            counter.add(value);
        }
        const int expected = (int)all.size();
        const int counter_result = counter.get_uniq_num();
        const double error = relative_error(expected, counter_result);
        all_errors.insert(error);
        printf(
                    "%d numbers in range [1 .. %d], %d uniq, %d result, %.5f "
                    "relative error\n",
                    N, tests[i], expected, counter_result, error);
        assert(error <= 0.1);
    }
    std::set<double>::iterator it = all_errors.begin();
    std::advance(it, all_errors.size() / 2);  // median
    std::cout << "median error: " << *it << std::endl;
}
