# Count-distinct problem

## Алгоритм
HyperLogLog (with practical optimizations) ([описание](http://algo.inria.fr/flajolet/Publications/FlFuGaMe07.pdf) и [ещё описание](https://storage.googleapis.com/pub-tools-public-publication-data/pdf/40671.pdf))

С оптимизацией хранения регистров, что позволило увеличить их количество и уменьшить относительную погрешность при <32Kb.

## Альтернативы
Probabilistic Counting, MinCount, LogLog, HyperLogLog(ориг.)

## Запуск
```
mkdir build
cd build
cmake ..
make
./test_simple
./test_extended
```

## Requirements
```  
gcc, c++17  
```
